from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re

class Instagram():
    def __init__(self, email, password, url_profile, browser='Chrome'):
        LOGIN_URL = 'https://www.instagram.com/accounts/login/'

        self.email = email
        self.password = password
        self.url_profile = url_profile
        self.post = None

        self.like_count_post = 0
        self.text_post = " "
        self.date_post = " "

        self.chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        self.chrome_options.add_experimental_option("prefs", prefs)
        mobile_emulation = {"deviceName": "Nexus 5"}
        self.chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)

        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=self.chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(1)

    def Login(self):

        email_element = self.driver.find_element_by_name('username')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_name('password')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_xpath("//div[contains(text(),'Log In')]")
        login_button.click()

        time.sleep(5)
        if self.check_exists_by_xpath("//button[contains(text(),'Save Info')]"):
            self.driver.find_element_by_xpath("//button[contains(text(),'Save Info')]").click()

        time.sleep(5)
        if self.check_exists_by_xpath("//button[contains(text(),'Cancel')]"):
            self.driver.find_element_by_xpath("//button[contains(text(),'Cancel')]").click()

    def Scraping(self):
        self.driver.get(self.url_profile)
        try:
            if self.check_exists_by_xpath("//div[@class='OfoBO']"):
                print("okkkkk")
                button_suggested = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='OfoBO']")))
                button_suggested.click()
                time.sleep(5)

                button_suggested = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//a[@class='HUW1v hUQXy']")))
                button_suggested.click()
                time.sleep(5)

                # scroll
                for i in range(1, 10):
                    print("o")
                    self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
                    time.sleep(5)
        except:
            print("Loading took too much time!")

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True
